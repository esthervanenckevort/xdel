.SUFFIXES: .c .obj 

.all: xdel.exe

.c.obj:
    @echo " Compile::C++ Compiler "
    icc.exe /Ss /O /Gm /C %s

xdel.exe: xdel.obj xdel.def
    @echo " Link::Linker "
    icc.exe @<<
     /B" /exepack:2 /packd /optfunc"
     /Fexdel.exe 
     os2386.lib 
     cppom30.lib 
     xdel.def
     xdel.obj
<<

xdel.obj: xdel.c xdel.h
